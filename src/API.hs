{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE TypeOperators  #-}

module API where

import           Data.Aeson
import           Models
import           Servant
import           Servant.API.Generic

data UserCreate = UserCreate
  { birthYear :: Integer
  , birthMonth :: Int
  , birthDay :: Int
  , secondsOfDay :: Integer
  , name :: String
  } deriving (Generic, FromJSON)

data UserResponse = UserResponse
  { allUsers :: [AppUser]
  } deriving (Generic, ToJSON)

data AppUser = AppUser
  { name :: String
  , sign :: SunSign
  } deriving (Generic, ToJSON)

data SunSign =
      Aries
    | Taurus
    | Gemini
    | Cancer
    | Leo
    | Virgo
    | Libra
    | Scorpio
    | Sagittarius
    | Capricorn
    | Aquarius
    | Pisces

instance ToJSON SunSign    

data UserApi route = UserApi
  { create :: route :- ReqBody '[JSON] UserCreate :> Post '[JSON] ()
  , list   :: route :- Get '[JSON] [UserResponse]
  } deriving (Generic)

data BaseApi route = BaseApi
  { user :: route :- "user" :> ToServantApi UserApi
  } deriving (Generic)

type Api = ToServantApi BaseApi
