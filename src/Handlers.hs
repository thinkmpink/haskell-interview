module Handlers where

import           API
import           Control.Monad.Except
import           Control.Monad.Reader
import           Database.Persist.Sql
import           Data.Time.Calendar
import           Data.Time.Clock
import           Models
import           Servant
import           Servant.API.Generic
import           Servant.Server.Generic

type AppT = ReaderT SqlBackend (ExceptT ServantErr IO)

baseApi :: ToServant BaseApi (AsServerT AppT)
baseApi = genericServerT BaseApi
  { user = userApi
  }

userApi :: ToServant UserApi (AsServerT AppT)
userApi = genericServerT UserApi
  { create = insertUser
  , list = fmap toAppUser getUsers
  }

insertUser :: UserCreate -> Handler ()
insertUser c = runDb $ insert $ User n $ UTCTime d t
  where n = name c
        d = fromGregorian $ birthYear c $ birthMonth c $ birthDay c
        t = secondsToDiffTime $ secondsOfDay c

toAppUser :: User -> AppUser
toAppUser u = AppUser (username u) (toSunSign $ birthDate u)

getUsers = runDb $ selectList [] []

toSunSign :: UTCTime -> SunSign
toSunSign t = undefined
 -- would want to move this logic more into the SunSign type
